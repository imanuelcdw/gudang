<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Chronos\Chronos;
/**
 * ItemIns Controller
 *
 * @property \App\Model\Table\ItemInsTable $ItemIns
 *
 * @method \App\Model\Entity\ItemIn[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemInsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $page = "item-ins";
        $this->set(compact('page'));
    }

    public function beforeRender(Event $event){
        $fakepage = "Barang Masuk";

        $this->set(compact('fakepage'));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $this->paginate = [
            'contain' => ['Items']
        ];
        $itemIns = $this->paginate($this->ItemIns);

        $this->set(compact('itemIns'));
    }

    /**
     * View method
     *
     * @param string|null $id Item In id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemIn = $this->ItemIns->get($id, [
            'contain' => ['Items']
        ]);

        $this->set('itemIn', $itemIn);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        $itemIn = $this->ItemIns->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if(empty($data['jumlah_data'])){
                $this->Flash->error(__('Tidak Ada Barang'));
                return $this->redirect(['action' => 'add']);
            }
            $itemIn = $this->ItemIns->patchEntities($itemIn, $data);
            if ($id != null) {
                $itemIn['item_id'] = $id;
            }
            foreach($itemIn as $value){
                $item = $this->ItemIns->Items->get($value['item_id']);
                $value['unit'] = $item['unit'];
                $datas[] = $value;
            }

            if ($this->ItemIns->saveMany($itemIn)) {
                $this->Flash->success(__('The item in has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item in could not be saved. Please, try again.'));
        }
        if(empty($id)){
            $items = $this->ItemIns->Items->find('list', ['limit' => 200]);
        }else{
            $items = $this->ItemIns->Items->get($id);
        }
        $this->set(compact('itemIn', 'items', 'id'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Item In id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemIn = $this->ItemIns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemIn = $this->ItemIns->patchEntity($itemIn, $this->request->getData());
            if ($this->ItemIns->save($itemIn)) {
                $this->Flash->success(__('The item in has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item in could not be saved. Please, try again.'));
        }
        $items = $this->ItemIns->Items->find('list', ['limit' => 200]);
        $this->set(compact('itemIn', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Item In id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $itemIn = $this->ItemIns->get($id);
        if ($this->ItemIns->delete($itemIn)) {
            $this->Flash->success(__('The item in has been deleted.'));
        } else {
            $this->Flash->error(__('The item in could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
