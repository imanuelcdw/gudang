<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($item) ?>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control','type'=>'text','label'=>'Nama Barang','autocomplete'=>'off','tabindex'=>'1']) ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('unit',['class'=>'form-control','label'=>'Satuan','autocomplete'=>'off','tabindex'=>'2']) ?>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('description',['class'=>'form-control','label'=>'Keterangan*','tabindex'=>'3']) ?>
                    <small class="float-right">(*optional)</small>
                </div>
            </div>
        </div>

        <div class="row">

        </div>

        <?= $this->Form->button('Simpan',['class'=>'btn btn-primary','tabindex'=>'4']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
