
<div class="card">
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <h1><?= $itemOut->has('item') ? $this->Html->link($itemOut->item->name, ['controller' => 'Items', 'action' => 'view', $itemOut->item->id]) : '' ?></h1>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Jumlah</label>
                    <div class="col-sm-9">
                        <?= h($itemOut->qty) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Satuan</label>
                    <div class="col-sm-9">
                        <?= h($itemOut->unit) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Keterangan</label>
                    <div class="col-sm-9">
                        <?= empty($itemOut->description)? '-Tanpa Keterangan-' : h($itemOut->description) ?>
                    </div>
                </div>
            </li>            


            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Tanggal Masuk</label>
                    <div class="col-sm-9">
                        <?= h($itemOut->created->format('d M Y H:i')) ?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>


