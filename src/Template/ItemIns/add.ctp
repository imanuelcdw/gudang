<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($itemIn) ?>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('jumlah_data',['type'=>'number', 'class'=>'form-control','label'=>'Jumlah Data','min'=>'0']) ?>
                </div>
            </div>
        </div>
        <hr>
        <!-- =============== -->
        <div id="data">
        </div>
        

            <?= $this->Form->button('Simpan',['class'=>'btn btn-primary']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<?php $this->start('script') ?>
    <script>
        $('#jumlah-data').on('keyup change' ,function(){
            $('#data').empty();
            var form = "";
            for (var no = 1 ; no <= $(this).val(); no++) {
                form +=
                "<h3> Barang "+ no +"</h3>"+
                "<div class='row'>"+
                    "<div class='col-md-4'>"+
                        "<div class='form-group'>"+
                            "<label>Nama Barang</label>"+
                            "<select name='"+no+"[item_id]' class='form-control' required>"+
                                "<option selected disabled>-- Silahkan Pilih --</option>"+
                                <?php foreach ($items as $keyI => $valueI): ?>
                                    "<option value='<?= $keyI ?>'><?= $valueI ?></option>"+
                                <?php endforeach ?>
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<div class='col-md-4'>"+
                        "<div class='form-group'>"+
                            "<label>Jumlah</label>"+
                            "<input type='number' name='"+no+"[qty]' class='form-control' required min='0' step='0.01'>"+
                        "</div>"+
                    "</div>"+
                    "<div class='col-md-4'>"+
                        "<div class='form-group'>"+
                            "<label>Harga</label>"+
                            "<input type='number' name='"+no+"[price]' class='form-control' required min='0'>"+
                        "</div>"+
                    "</div>"+
                "</div>"+
                "<hr>"
                ;
            }
            $('#data').append(form);
            $('select').select2();
        });
    </script>
<?php $this->end() ?>