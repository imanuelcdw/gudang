<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $name
 * @property int $stock
 * @property string $unit
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\ItemIn[] $item_ins
 * @property \App\Model\Entity\ItemOut[] $item_outs
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'stock' => true,
        'unit' => true,
        'description' => true,
        'created' => true,
        'item_ins' => true,
        'item_outs' => true
    ];
}
