<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
/**
 * Wizard Controller
 *
 *
 * @method \App\Model\Entity\Wizard[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WizardController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        
        $items = TableRegistry::get('Items');
    }

    public function step1()
    {
        $subpage = 'Add Barang';

        $this->set(compact('subpage'));
    }

   
}
