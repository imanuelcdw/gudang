<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * ItemOuts Controller
 *
 * @property \App\Model\Table\ItemOutsTable $ItemOuts
 *
 * @method \App\Model\Entity\ItemOut[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemOutsController extends AppController
{
    public function beforeRender(Event $event){
        $fakepage = "Barang Keluar";

       
        $this->set(compact('fakepage'));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Items']
        ];
        $itemOuts = $this->paginate($this->ItemOuts);

        $this->set(compact('itemOuts'));
    }

    /**
     * View method
     *
     * @param string|null $id Item Out id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemOut = $this->ItemOuts->get($id, [
            'contain' => ['Items']
        ]);

        $this->set('itemOut', $itemOut);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        $itemOut = $this->ItemOuts->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $itemOut = $this->ItemOuts->patchEntities($itemOut, $data);
            foreach($itemOut as $value){
                $item = $this->ItemOuts->Items->get($value['item_id']);
                $value['unit'] = $item['unit'];
                $datas[] = $value;
            }
            if ($this->ItemOuts->saveMany($itemOut)) {
                $this->Flash->success(__('The item out has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item out could not be saved. Please, try again.'));
        }
        if(empty($id)){
            $items = $this->ItemOuts->Items->find();
        }else{
            $items = $this->ItemOuts->Items->get($id);
        }

        $this->set(compact('itemOut', 'items','id'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Item Out id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemOut = $this->ItemOuts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemOut = $this->ItemOuts->patchEntity($itemOut, $this->request->getData());
            if ($this->ItemOuts->save($itemOut)) {
                $this->Flash->success(__('The item out has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item out could not be saved. Please, try again.'));
        }
        $items = $this->ItemOuts->Items->find('list', ['limit' => 200]);
        $this->set(compact('itemOut', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Item Out id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $itemOut = $this->ItemOuts->get($id);
        if ($this->ItemOuts->delete($itemOut)) {
            $this->Flash->success(__('The item out has been deleted.'));
        } else {
            $this->Flash->error(__('The item out could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
