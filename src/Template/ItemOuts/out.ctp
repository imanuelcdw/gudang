<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($itemOut) ?>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('item_id',['class'=>'form-control','label'=>'Nama Barang']) ?>    
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <small class="form-text float-right">Stock : <?= $stock.' '.$type ?></small>
                    <?= $this->Form->control('qty',['class'=>'form-control','max'=>$stock, 'min'=>'1','label'=>'Jumlah']) ?>    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('type',['class'=>'form-control','value'=>$type,'label'=>'Satuan']) ?>
                </div>   
            </div>
        </div>
        
        <?= $this->Form->button('Simpan',['class'=>'btn btn-warning']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>

