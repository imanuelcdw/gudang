<div class="card">
    <?php if($this->request->query('layout') == "no"){?>
        <?= $this->Html->css('../modules/bootstrap/css/bootstrap.min.css') ?>
        <?= $this->Html->css('../modules/fontawesome/css/all.min.css') ?>
        <style>
            table th, table td{
                border: 1px solid #3c3c3c;
                padding: 3px 8px;
            }
        </style>
        <h1 class="text-center">Laporan Barang Masuk</h1>
    <?php }?>
    <?php if($this->request->query('layout') != "no"){?>
    	<div class="card-header">
            <a href="<?= $this->Url->Build(['?' => ['print' => 'ok', 'layout' => 'no']])?><?= @$_POST['year'] ? "&month=$_POST[month]&year=$_POST[year]" : '' ?>" class="btn btn-primary" target="blank"><i class="fa fa-print"></i> Print</a>&nbsp;
            <a href="<?= $this->Url->Build(['?' => ['excel' => 'ok', 'layout' => 'no']])?><?= @$_POST['year'] ? "&month=$_POST[month]&year=$_POST[year]" : '' ?>" class="btn btn-success" target="blank"><i class="fa fa-print"></i> Excel</a>&nbsp;
            <?= $this->Form->create() ?>
                <div class="input-group">
                    <?= $this->Form->control('month',['options' => $month, 'empty' => 'Bulan', 'class'=>'form-control ml-5', 'label' => false]) ?>
                    <select class="form-control ml-5" name="year" id="year">
                        <option value="<?= @$_POST['year']?>"><?= ( @$_POST['year'] ? $_POST['year'] : 'Tahun') ?></option>
                        <?php $tahun = date('Y'); $jml_tahun = $tahun - 2016 ?>
                        <?php for($i = 0; $i < $jml_tahun; $i++){?>
                            <option value="<?= $tahun?>"><?= $tahun--?></option>
                        <?php }?>
                    </select>
                    <?= $this->Form->button('Cetak',['class'=>'btn btn-primary']) ?>
                </div>
            <?= $this->Form->end() ?>
        </div>
    <?php }?>
	<div class="card-body">
		<div class="table-responsive">
          <table class="table table-striped" id="table-1" <?= ($this->request->query('excel') != "ok" ? "border='0'" : "border='1'")?>>
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jumlah</th>
                <th>Tanggal</th>
              </tr>
            </thead>
            <tbody>
                <?php $no = 1 ?>
                <?php foreach($items as $value): ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= ($value->has('item_id') ? $value->item_name : ' ') ?></td>
                        <td><?= $value->qty.' '.$value->unit ?></td>
                        <td><?= $value->created->nice() ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
          </table>
        </div>
	</div>
</div>
<?php if($this->request->query('print') == "ok"){?>
    <script>window.print()</script>
<?php }?>