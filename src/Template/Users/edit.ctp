<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($user) ?>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control','type'=>'text','label'=>'Nama']) ?>    
                </div>
                
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('password',['class'=>'form-control','label'=>'Password Baru']) ?>    
                </div>
            </div>
        </div>

        <div class="row">
            
        </div>

        
        <?= $this->Form->button('Update',['class'=>'btn btn-primary']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
