<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

$cakeDescription = 'Gudang Kantin';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('../modules/bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('../modules/fontawesome/css/all.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('components.css') ?>
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body>
    
    <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
                <h1>Gudang</h1>
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Login</h4></div>

              <div class="card-body">
                <?= $this->Form->create() ?>

                  <div class="form-group">
                    <label for="email">Username</label>
                    <?= $this->Form->control('username', ['class'=>'form-control','autocomplete'=>'off','label'=>false,'tabindex'=>'1']); ?>
                    <div class="invalid-feedback">
                      Please fill in your email
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="d-block">
                        <label for="password" class="control-label">Password</label>
                    </div>
                    <?= $this->Form->control('password', ['class'=>'form-control','autocomplete'=>'off','label'=>false,'tabindex'=>'2']); ?>
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                  </div>

                  <div class="form-group">
                    <?= $this->Form->button('Login',['class'=>'btn btn-primary btn-lg btn-block','tabindex'=>'3']) ?>
                  </div>

                <?= $this->Form->end() ?>
                
              
              </div>
            </div>
            

            <?= $this->Flash->render() ?>
          </div>
        </div>
      </div>
    </section>
  </div>

<?= $this->Html->Script('../modules/jquery.min.js') ?>
<?= $this->Html->Script('../modules/popper.js') ?>
<?= $this->Html->Script('../modules/tooltip.js') ?>
<?= $this->Html->Script('../modules/bootstrap/js/bootstrap.min.js') ?>
<?= $this->Html->Script('../modules/nicescroll/jquery.nicescroll.min.js') ?>
<?= $this->Html->Script('stisla.js') ?>
<?= $this->Html->Script('scripts.js') ?>
<?= $this->Html->Script('custom.js') ?>


</body>
</html>
