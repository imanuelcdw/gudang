
<div class="card">
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <h1><?= $itemIn->has('item') ? $this->Html->link($itemIn->item->name, ['controller' => 'Items', 'action' => 'view', $itemIn->item->id]) : '' ?></h1>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Jumlah</label>
                    <div class="col-sm-9">
                        <?= h($itemIn->qty) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Satuan</label>
                    <div class="col-sm-9">
                        <?= h($itemIn->unit) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Harga</label>
                    <div class="col-sm-9">
                        <?= 'Rp.'.$this->Number->format(h($itemIn->price)).',-' ?>
                    </div>
                </div>
            </li>


            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Keterangan</label>
                    <div class="col-sm-9">
                        <?= empty($itemIn->description)? '-Tanpa Keterangan-' : h($itemIn->description) ?>
                    </div>
                </div>
            </li>            


            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Tanggal Masuk</label>
                    <div class="col-sm-9">
                        <?= h($itemIn->created->format('d M Y H:i')) ?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>


