<div class="card">
    <div class="card-body">
        <h2>Steps </h2>
        <ul class="nav nav-pills flex-column" id="myTab4" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab4" data-toggle="tab" href="#home4" role="tab" aria-controls="home" aria-selected="true">
              <span class="badge-light badge-pill float-right">1</span>Home
            </a>
          </li>
          <br>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab4" data-toggle="tab" href="#profile4" role="tab" aria-controls="profile" aria-selected="false">
              <span class="badge-light badge-pill float-right">2</span>Profile
            </a>
          </li>
          <br>
          <li class="nav-item">
            <a class="nav-link" id="contact-tab4" data-toggle="tab" href="#contact4" role="tab" aria-controls="contact" aria-selected="false">
              <span class="badge-light badge-pill float-right">3</span>Contact
            </a>
          </li>
        </ul>
    </div>
</div>

<?php $this->start('css') ?>
    <style>
    .nav-item .active{
      background-color: rgb(255,164,34) !important;
      color: white !important;
    }
    .nav-item a{
      color: rgb(255,164,34) !important;
    }
    </style>
<?php $this->end() ?>
