-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 02, 2019 at 01:18 PM
-- Server version: 5.7.19
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `warehouse`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `stock` double NOT NULL DEFAULT '0',
  `unit` varchar(100) NOT NULL DEFAULT 'kg',
  `description` text,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `stock`, `unit`, `description`, `created`) VALUES
(5, 'Beras', 0.5, 'kg', '', '2019-03-05 08:42:50'),
(6, 'Beras Merah', 5, 'kg', '', '2019-03-05 08:43:03');

-- --------------------------------------------------------

--
-- Table structure for table `item_ins`
--

DROP TABLE IF EXISTS `item_ins`;
CREATE TABLE `item_ins` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `description` text,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_ins`
--

INSERT INTO `item_ins` (`id`, `item_id`, `qty`, `unit`, `price`, `description`, `created`) VALUES
(5, 6, 5, 'kg', 10000, '', '2019-03-05 08:43:37'),
(6, 5, 5, 'kg', 10000, '', '2019-03-05 08:44:18');

--
-- Triggers `item_ins`
--
DROP TRIGGER IF EXISTS `create_item_in`;
DELIMITER $$
CREATE TRIGGER `create_item_in` AFTER INSERT ON `item_ins` FOR EACH ROW BEGIN

UPDATE items SET stock = stock + new.qty WHERE id = new.item_id;

END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `delete_item_in`;
DELIMITER $$
CREATE TRIGGER `delete_item_in` AFTER DELETE ON `item_ins` FOR EACH ROW BEGIN

UPDATE items SET stock = stock - old.qty WHERE id = old.item_id;

END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `edit_item_in`;
DELIMITER $$
CREATE TRIGGER `edit_item_in` AFTER UPDATE ON `item_ins` FOR EACH ROW BEGIN 

UPDATE items SET stock = stock - old.qty WHERE id = old.item_id;

UPDATE items SET stock = stock + new.qty WHERE id = new.item_id;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `item_outs`
--

DROP TABLE IF EXISTS `item_outs`;
CREATE TABLE `item_outs` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(100) NOT NULL,
  `description` text,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_outs`
--

INSERT INTO `item_outs` (`id`, `item_id`, `qty`, `unit`, `description`, `created`) VALUES
(2, 5, 2, 'kg', '', '2019-03-05 08:49:07'),
(4, 5, 2.5, 'kg', NULL, '2019-05-02 00:00:00');

--
-- Triggers `item_outs`
--
DROP TRIGGER IF EXISTS `create_item_out`;
DELIMITER $$
CREATE TRIGGER `create_item_out` AFTER INSERT ON `item_outs` FOR EACH ROW BEGIN

UPDATE items SET stock = stock - new.qty WHERE id = new.item_id;

END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `delete_item_out`;
DELIMITER $$
CREATE TRIGGER `delete_item_out` AFTER DELETE ON `item_outs` FOR EACH ROW BEGIN 

UPDATE items SET stock = stock + old.qty WHERE id = old.item_id;

END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `edit_item_out`;
DELIMITER $$
CREATE TRIGGER `edit_item_out` AFTER UPDATE ON `item_outs` FOR EACH ROW BEGIN

UPDATE items SET stock = stock + old.qty WHERE id = old.item_id;

UPDATE items SET stock = stock - new.qty WHERE id = new.item_id;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `created`) VALUES
(1, 'Administratoooor', 'administrator', '$2y$10$0WH5E.yQ8RZaF9IzwQdaw.uSgOHXzv7UNwWVAWyHuaoWyvHw1uXB6', '2019-03-05 08:08:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_ins`
--
ALTER TABLE `item_ins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_outs`
--
ALTER TABLE `item_outs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `item_ins`
--
ALTER TABLE `item_ins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `item_outs`
--
ALTER TABLE `item_outs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
