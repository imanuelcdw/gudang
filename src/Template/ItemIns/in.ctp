<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($itemIn) ?>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control','type'=>'text','autocomplete'=>'off','label'=>'Name Barang','value'=>$name]) ?>    
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('qty',['class'=>'form-control','label'=>'Jumlah']) ?>    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('type',['class'=>'form-control','label'=>'Satuan','type'=>'text','autocomplete'=>'off','value'=>$type]) ?>    
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('price',['class'=>'form-control','label'=>'Harga']) ?>    
                </div>
            </div>
        </div>

        
        <?= $this->Form->button('Simpan',['class'=>'btn btn-warning']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
