<!-- Sidebar -->
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
            <div class="sidebar-brand">
                <a href="#">Gudang Kantin</a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="#">GK</a>
            </div>
            <ul class="sidebar-menu">
                <!-- Main Menu -->
                <li class="menu-header">Dashboard</li>
                <!-- <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="dashboard-general.html">General Dashboard</a></li>
                        <li><a class="nav-link" href="dashboard-ecommerce.html">Ecommerce Dashboard</a></li>
                    </ul>
                </li> -->

                <li class="<?= $page == 'dashboard' ? 'active' : '' ?>">
                  <a href="<?= $this->Url->Build(['controller'=>'Pages','action'=>'dashboard']) ?>" class="nav-link">
                    <i class="fas fa-fire"></i>
                    <span>Dashboard</span>
                  </a>
                </li>

                <!-- <li class="<?= $page == 'users' ? 'active' : '' ?>">
                  <a href="<?= $this->Url->Build(['controller'=>'Users','action'=>'index']) ?>" class="nav-link">
                    <i class="fas fa-users"></i>
                    <span>Users</span>
                  </a>
                </li> -->


                <li class="menu-header">Main</li>

                <li class="<?= $page == 'items' ? 'active' : '' ?>">
                  <a href="<?= $this->Url->Build(['controller'=>'Items','action'=>'index']) ?>" class="nav-link">
                    <i class="fas fa-cube"></i>
                    <span>Barang</span>
                  </a>
                </li>

                <li class="<?= $page == 'item-ins' ? 'active' : '' ?>">
                  <a href="<?= $this->Url->Build(['controller'=>'Item_ins','action'=>'index']) ?>" class="nav-link">
                    <i class="fas fa-sign-in-alt"></i>
                    <span>Barang Masuk</span>
                  </a>
                </li>

                <li class="<?= $page == 'item-outs' ? 'active' : '' ?>">
                  <a href="<?= $this->Url->Build(['controller'=>'Item_outs','action'=>'index']) ?>" class="nav-link">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Barang Keluar</span>
                  </a>
                </li>
                

                <li class="menu-header">Report</li>
                <li class="<?= $page == 'report_in' ? 'active' : '' ?>">
                  <a href="<?= $this->Url->Build(['controller'=>'Reports','action'=>'report_in']) ?>" class="nav-link">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Laporan Barang Masuk</span>
                  </a>
                </li>

                <li class="<?= $page == 'report_out' ? 'active' : '' ?>">
                  <a href="<?= $this->Url->Build(['controller'=>'Reports','action'=>'report_out']) ?>" class="nav-link">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Laporan Barang Keluar</span>
                  </a>
                </li>
            </ul>

        </aside>
      </div>
