<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Helper;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// require ROOT.DS. 'vendor' .DS. 'phpoffice/phpspreadsheet/src/Bootstrap.php' ;

/**
 * Reports Controller
 *
 *
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReportsController extends AppController
{

    public function beforeRender(Event $event)
    {
        $report = 'report';
        $month = [
            '1' => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desemver'
        ];

        $this->set(compact('report','month'));
    }

    public function reportIn(){
        $page = 'report_in';
        $fakepage = 'Laporan Barang Masuk';
        $subpage = null;

        $this->loadModel('ItemIns');
        $months = $this->request->getQuery('month');
        $year = $this->request->getQuery('year');
        if ($months && $year){
            $items = $this->ItemIns->find()
            ->select(['id','item_id','qty','price','created','item_name' => 'items.name'])
            ->leftJoin(['Items' => 'items'],["ItemIns.item_id = Items.id"])
            ->where("MONTH(ItemIns.created) = '$months' AND YEAR(ItemIns.created) = '$year';");
        }else{
            $items = $this->ItemIns->find()
            ->select(['id','item_id','qty','price','created','item_name' => 'items.name','unit' => 'items.unit'])
            ->leftJoin(['Items' => 'items'],['ItemIns.item_id = Items.id']);
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $months = empty($data['month']) ? '' : $data['month'];
            $year = empty($data['year']) ? '' : $data['year'];

            if (!$months || !$year){
                $this->Flash->error(__('Bulan atau tahun masih kosong!'));

                return $this->redirect(['action' => 'reportIn']);
            }

            // $items = $this->ItemIns->find()->where("MONTH(created) = '$months' AND YEAR(created) = '$year';");
            $items = $this->ItemIns->find()
            ->select(['id','item_id','qty','price','created','item_name' => 'items.name'])
            ->leftJoin(['Items' => 'items'],["ItemIns.item_id = Items.id"])
            ->where("MONTH(ItemIns.created) = '$months' AND YEAR(ItemIns.created) = '$year';");
        }

        if ($this->request->query('print') == "ok") {
            $this->layout = false;
            // $this->Flash->error(__('Bulan atau tahun masih kosong!'));
        }
        if($this->request->query('excel') == "ok"){
            $this->layout = false;
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=Laporan_barang_masuk_".date('d-m-Y').".xls");
        }

        $this->set(compact('fakepage','page','subpage','items'));
    }

    public function reportOut(){
        $page = 'report_out';
        $fakepage = 'Laporan Barang Keluar';
        $subpage = null;

        $this->loadModel('ItemOuts');
        $months = $this->request->getQuery('month');
        $year = $this->request->getQuery('year');
        if ($months && $year){
            $items = $this->ItemOuts->find()
            ->select(['id','item_id','qty','created','item_name' => 'items.name'])
            ->leftJoin(['Items' => 'items'],["ItemOuts.item_id = Items.id"])
            ->where("MONTH(ItemOuts.created) = '$months' AND YEAR(ItemOuts.created) = '$year';");
        }else{
            $items = $this->ItemOuts->find()
            ->select(['id','item_id','qty','created','item_name' => 'items.name','unit' => 'items.unit'])
            ->leftJoin(['Items' => 'items'],['ItemOuts.item_id = Items.id']);
        }


        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $months = $data['month'];
            $year = $data['year'];

            if (!$months || !$year){
                $this->Flash->error(__('Bulan atau tahun masih kosong!'));

                return $this->redirect(['action' => 'reportIn']);
            }

            // $items = $this->ItemOuts->find()->contain(['Items'])->where("MONTH(created) = '$months' AND YEAR(created) = '$year';");
            $items = $this->ItemOuts->find()
            ->select(['id','item_id','qty','created','item_name' => 'items.name'])
            ->leftJoin(['Items' => 'items'],["ItemOuts.item_id = Items.id"])
            ->where("MONTH(ItemOuts.created) = '$months' AND YEAR(ItemOuts.created) = '$year';");
        }

        if ($this->request->query('print') == "ok") {
            $this->layout = false;
            // $this->Flash->error(__('Bulan atau tahun masih kosong!'));
        }
        if($this->request->query('excel') == "ok"){
            $this->layout = false;
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=Laporan_barang_keluar_".date('d-m-Y').".xls");
        }

        $this->set(compact('page','fakepage','subpage','items'));
    }

    public function print(){
        
    }
}
