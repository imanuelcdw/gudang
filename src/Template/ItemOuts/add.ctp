<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($itemOut) ?>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('jumlah_data',['type'=>'number', 'class'=>'form-control','label'=>'Jumlah Data', "min"=>'0']) ?>
                </div>
            </div>
        </div>
        <hr>
        <div id="data">
        </div>
        
        <?= $this->Form->button('Simpan',['class'=>'btn btn-primary']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
<?php $this->start('script') ?>
    <script>
        var angka = 0;
        var items = "<option selected disabled>-- Silahkan Pilih --</option>";
        <?php foreach ($items as $keyI => $valueI): ?>
            items += "<option stock='<?= $valueI->stock ?>' value='<?= $valueI->id ?>'><?= $valueI->name ?></option>";
        <?php endforeach ?>
        console.log(items);
        $('body').on('change','select',function(){
            var e = $(this);
            var target1 = $('.f-'+e.attr('attrname')+' small');
            var target2 = $('.f-'+e.attr('attrname')+' input');
            var stock = $('#s-'+e.attr('attrname')+' option:selected').attr('stock');
            target1.html('stock '+stock);
            target2.attr('max', stock);
            for (var i = angka; i >= 1; i--) {
                if (e.attr('attrname') != i) {
                    $("#s-" + i + " option[value='"+e.val()+"']").remove();
                }
            }
        });
        $('#jumlah-data').on('keyup change' ,function(){
            $('#data').empty();
            var form = "";
            for (var no = 1 ; no <= $(this).val(); no++) {
                form +=
                "<h3>Barang "+ no +"</h3>"+
                "<div class='row'>"+
                    "<div class='col-md-4'>"+
                        "<div class='form-group'>"+
                            "<label>Nama Barang</label>"+
                            "<select attrname='"+no+"' id='s-"+no+"' name='"+no+"[item_id]' class='form-control' required>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<div class='col-md-4'>"+
                        "<div class='form-group f-"+no+"'>"+
                            "<label>Jumlah</label>"+
                            "<input type='number' name='"+no+"[qty]' class='form-control' required min='0' step='0.01'>"+
                            "<small></small>"+
                        "</div>"+
                    "</div>"+
                "</div>"
                ;
            }
            angka = no - 1;
            $('#data').append(form);
            $('select').select2();
            $('select').append(items);
        });
    </script>
<?php $this->end() ?>