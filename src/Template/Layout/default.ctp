
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Gudang Kantin | <?= ucfirst(!empty($fakepage) ? $fakepage : $page) ?></title>

  <?= $this->element('css') ?>
  <?= $this->fetch('css') ?>
  
</head>

<body>
  <div id="preloader">
  </div>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">


    <?= $this->element('navbar') ?>
    <?= $this->element('sidebar') ?>



      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">

            <button onclick="window.history.back();" class="btn btn-primary mr-3" data-toggle="tooltip" data-placement="top" title="" data-original-title="Kembali"><i class="fa fa-chevron-left"></i></button>

            <h1><?= !empty($subpage) ? ucfirst($subpage) : '' ?> <?= !empty($fakepage) ? $fakepage : ucfirst($page) ?> </h1>

            <?= $this->element('breadcrumb') ?>
          </div>

          <div class="section-body">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
          </div>


        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
        </div>
        <div class="footer-right">
          v2.1.0
        </div>
      </footer>
    </div>
  </div>


<?= $this->element('js') ?>
<?= $this->fetch('script') ?>
</body>
</html>
