<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($itemIn) ?>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('item_id', ['options' => $items,'label'=>'Nama Barang','class'=>'form-control']); ?>
                </div>

            </div>
            <div class="col-md-4">
                <div class="form-group">

                    <?= $this->Form->control('qty',['class'=>'form-control','label'=>'Jumlah','min'=>'0']) ?>
                    <!-- <small class="float-right">Default 0</small>    -->
                </div>

            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('price',['class'=>'form-control','label'=>'Harga','type'=>'number', 'min'=>'0'])   ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('unit',['class'=>'form-control','label'=>'Satuan','autocomplete'=>'off']) ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('description',['class'=>'form-control','label'=>'Keterangan*']) ?>
                </div>
            </div>
        </div>

        <div class="row">

        </div>

        <?= $this->Form->button('Simpan',['class'=>'btn btn-primary']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
