<?php if($page != 'dashboard'): ?>
	<div class="section-header-breadcrumb">
	  <div class="breadcrumb-item"><a href="<?= $this->Url->Build('pages/dashboard') ?>" class="text-primary">Dashboard</a></div>

		<?php if(!empty($subpage)): ?>
			<div class="breadcrumb-item"><a href="<?= $this->Url->Build(['action'=>'index']) ?>" class="text-primary"><?= !empty($fakepage) ? $fakepage : ucfirst($page) ?></a></div>
	  		<div class="breadcrumb-item"><?= ucfirst($subpage) ?></div>
		<?php else: ?>
			<div class="breadcrumb-item"><?= !empty($fakepage) ? $fakepage : ucfirst($page) ?></div>
		<?php endif; ?>
	</div>
<?php endif; ?>
