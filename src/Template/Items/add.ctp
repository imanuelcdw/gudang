<?php $this->start('css') ?>
     <?= $this->Html->css('easy-autocomplete.min.css') ?>
     <?= $this->Html->css('easy-autocomplete.themes.min.css') ?>
<?php $this->end() ?>

<?php $this->start('script') ?>
    <?= $this->Html->script('jquery.easy-autocomplete.min.js') ?>   
     <script>
         var options2 = {
            data: [
                'kg',
                'pcs',
                'buah',
                'liter'
            ],
            list: {
                match: {
                    enabled: false
                }
            }
        };

        var options1 = {
            data:
                {"Barang" : [
                    <?php foreach($items as $value): ?>
                        '<?= $value->name ?>',
                    <?php endforeach; ?>
                    ]
                },
            categories: [
                { 
                    listLocation: "Barang",
                    header: "-- Barang yang sudah ada --"
                }, 
            ],
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            }
        };
        $("#name").easyAutocomplete(options1); 
        $("#unit").easyAutocomplete(options2); 
     </script>
<?php $this->end() ?>

<div class="card">
    <div class="card-body"><br>
        <?= $this->Form->create($item) ?>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control ','type'=>'text','label'=>'Nama Barang','autocomplete'=>'off','tabindex'=>'1']) ?>
                    <small id="name-desc" class="text-danger d-none">
                        Barang Sudah Ada
                    </small>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('unit',['required', 'class'=>'form-control','label'=>'Satuan','autocomplete'=>'off','tabindex'=>'2','value'=>'']) ?>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->control('description',['class'=>'form-control','label'=>'Keterangan*','tabindex'=>'3']) ?>
                    <small class="float-right">(*optional)</small>
                </div>
            </div>
        </div>

        <div class="row">

        </div>

        <?= $this->Form->button('Simpan',['class'=>'btn btn-primary','tabindex'=>'4']) ?>


        <?= $this->Form->end() ?>
    </div>
</div>
