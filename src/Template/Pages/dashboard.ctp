<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="card-body">
				<h2 class="text-center">Jumlah Barang</h2>
				<h1 class="display-4 text-center"><?= $items_count ?></h1>
				<!-- <a href="<?= $this->Url->Build(['controller'=>'Items','action'=>'index']) ?>" class="btn btn-block btn-sm btn-warning">Lihat</a> -->
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card">
			<div class="card-body">
				<h2 class="text-center">Barang Masuk hari ini</h2>
				<h1 class="display-4 text-center"><?= $itemIns_count ?></h1>
				<!-- <a href="<?= $this->Url->Build(['controller'=>'ItemIns','action'=>'index']) ?>" class="btn btn-block btn-sm btn-warning">Lihat</a> -->
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card">
			<div class="card-body">
				<h2 class="text-center">Barang Keluar hari ini</h2>
				<h1 class="display-4 text-center"><?= $itemOuts_count ?></h1>
				<!-- <a href="<?= $this->Url->Build(['controller'=>'ItemOuts','action'=>'index']) ?>" class="btn btn-block btn-sm btn-warning">Lihat</a> -->
			</div>
		</div>
	</div>

	
</div>
