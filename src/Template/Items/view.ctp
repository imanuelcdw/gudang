<!-- CSS -->
<?php $this->start('css') ?>
    <?= $this->Html->css('../modules/datatables/datatables.min.css') ?>
    <?= $this->Html->css('../modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') ?>
    <?= $this->Html->css('../modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') ?>
<?php $this->end() ?>

<!-- JS -->
<?php $this->start('script') ?>
    <?= $this->Html->script('../modules/datatables/datatables.min.js') ?>
    <?= $this->Html->script('../modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') ?>
    <?= $this->Html->script('../modules/datatables/Select-1.2.4/js/dataTables.select.min.js') ?>
    <?= $this->Html->script('../modules/jquery-ui/jquery-ui.min.js') ?>
    <?= $this->Html->script('page/modules-datatables.js') ?>
<?php $this->end() ?>


<div class="card">
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <h1><?= h($item->name) ?></h1>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Stok</label>
                    <div class="col-sm-9">
                        <?= h($item->stock) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Satuan</label>
                    <div class="col-sm-9">
                        <?= h($item->unit) ?>
                    </div>
                </div>
            </li>

            <li class="list-group-item">
                <div class="row">
                    <label class="font-weight-bold col-sm-2">Tanggal Buat</label>
                    <div class="col-sm-9">
                        <?= h($item->created) ?>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
        <div class="card-body">
            <h3>Barang Masuk</h3>
            <div class="table-responsive">
                <table class="table table-striped" id="table-2" border="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                            <th>Tanggal Masuk</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; $jumlah=0; ?>
                        <?php foreach ($item->item_ins as $itemIns): ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= h($itemIns->qty) ?></td>
                            <td><?= h($itemIns->unit) ?></td>
                            <td><?= empty($itemIns->description)? '-Tanpa Keterangan-' : h($itemIns->description) ?></td>
                            <td><?= h($itemIns->created) ?></td>
                        </tr>
                        <?php $jumlah = $jumlah + $itemIns->qty ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
        <div class="card-body">
            <h3>Barang Keluar</h3>
            <div class="table-responsive">
                <table class="table table-striped" id="table-3" border="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                            <th>Tanggal Masuk</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; $jumlah=0; ?>
                        <?php foreach ($item->item_outs as $itemOuts): ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= h($itemOuts->qty) ?></td>
                            <td><?= h($itemOuts->unit) ?></td>
                            <td><?= empty($itemOuts->description)? '-Tanpa Keterangan-' : h($itemOuts->description) ?></td>
                            <td><?= h($itemOuts->created) ?></td>
                        </tr>
                        <?php $jumlah = $jumlah + $itemOuts->qty ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
